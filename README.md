About this repository
=====================

This repository contains the source code of the *__Data-bag__ file
synchronization tool*. Its top-level components are:

        src/           Data-bag source files.
        lib/           Run-time libraries used by Data-bag.
        LICENSE/       Legal documents with the project's licensing terms.
        NOTICE.md      A summary of licenses that apply to Data-bag with
                        references to detailed legal documents.
        build.xml      Configuration file for the tool (Ant) that builds the
                        project's executable and other artifacts.
        ivy.xml        Descriptor of this project for dependency management.
        .classpath     Eclipse configuration file for the project.
        .project       Eclipse configuration file for the project.
        docs/          The project's documentation.
        distfiles/     Addtional files placed in the distribution archive.
        test/          Source files of the project's regression tests.

Building a *Data-bag* executable
================================ 

To build a *Data-bag* executable from these sources, you need:

   - A Java SDK, also known as JDK, Standard Edition (SE), version 8 or
   later, available from OpenJDK <http://openjdk.java.net/> or Oracle
   <https://www.oracle.com/technetwork/java/javase/downloads/index.html>
   
   Even though a Java runtime may already be installed on your machine
   (check that by running `java -version`), the build will fail if you
   don't have a complete JDK (check that by running `javac`).

   - Apache Ant version 1.9.16 or newer, available from the Apache Software
   Foundation <https://ant.apache.org/>.

   - Apache Ivy version 2.5.2 or newer (with its dependencies),
   available from the Apache Software Foundation <http://ant.apache.org/ivy/>.
   If you don't have it installed into your Ant instance, the
   build process can download and install Ivy for you.
   But if you have it installed elsewhere, or prefer a manual
   install, set the `IVY_HOME` environment variable to point to its location
   before proceeding.

While the build process may work with older versions of Apache Ant and Ivy,
they are known to have security issues, which may compromise the system you
are building *Data-bag* on, as well as the resulting artifacts.

We further assume that you have properly configured `JAVA_HOME` and `ANT_HOME`
environment variables and that you can run both `ant` and `java` without
prefixes on the command line. You may want to tweak the following commands
if you have a different configuration.

To build a *Data-bag* executable, change dir to the working copy of this
repository (_working tree_), and run ANT:

        ant

The result is a JAR file `databag.jar` with statically-linked runtime
libraries.


Running *Data-bag*
==================

As of now, the only interface to *Data-bag* is command line. The
statically-linked `databag.jar` built using the above instructions does not
require any additional Java configuration to run. For example, to test a
*Data-bag* binary built at the root of your working tree, run:

        java -jar databag.jar -?

You should see verbose output explaining the tool's command line syntax and
describing most of its options. Please refer to the
[*Data-bag* manual](https://git.coop/data-bag/store/-/blob/1.x/docs/manual.md)
for further information.

Hacking *Data-bag*
==================

Once you have a clone of this repository, you can import it into Eclipse
using the enclosed `.classpath` and `.project` files. You may have to update
source and javadoc locations for the runtime libraries. To compile and
run tests for the project, you'll have to download *JUnit 4.5* or newer from
<http://www.junit.org> and add them as a library to your build path. To
generate javadoc for the project, use ANT:

        ant javadoc

Then, navigate to file `index.html` in the `javadoc` directory of your working
tree to browse the _javadoc_.

<!--
Contacting the project's team
=============================

You can send a message to the project's team by email

or via the [project's page on Git.coop](https://git.coop/data-bag).
-->
