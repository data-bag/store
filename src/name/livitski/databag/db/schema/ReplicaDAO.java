/**
 *  Copyright 2010-2013, 2023 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.db.schema;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import name.livitski.databag.db.AbstractDAO;
import name.livitski.databag.db.Cursor;
import name.livitski.databag.db.DBException;
import name.livitski.databag.db.IncompatibleSchemaException;
import name.livitski.databag.db.Manager;
import name.livitski.databag.db.NoSuchRecordException;
import name.livitski.databag.db.PreparedStatementCursor;
import name.livitski.databag.db.PreparedStatementHandler;
import name.livitski.databag.db.SchemaUpgrades;

/**
 * DAO implementation for the <code>Replica</code> table.
 * @see ReplicaDTO
 */
public class ReplicaDAO extends AbstractDAO
{
 /**
  * Looks up the default replica information for a (foruser, fromhost) pair,
  * assuming the {@link ReplicaDTO#DEFAULT_SCHEME local file scheme} and
  * empty authority. TODO: document the non-default scheme/authority mode.
  * If no replica has been designated as such, an existing replica
  * that has been created first is considered the default replica.
  * @param foruser user name on the machine hosting the bag
  * @param fromhost canonical host name of the machine hosting the bag
  * @return object that represents the default replica
  * record or <code>null</code> if there are no replicas for specified
  * user and host
  * @throws DBException if there is an error accessing the database
  */
 public ReplicaDTO findDefaultReplica(String foruser, String fromhost)
 	throws DBException
 {
  Loader loader = new Loader(mgr, foruser, fromhost, ReplicaDTO.DEFAULT_SCHEME, "");
  loader.execute();
  return loader.getReplica();
 }

 /**
  * Looks up replica information for the (foruser, fromhost, path) tuple.
  * @param foruser user name on the machine hosting the bag
  * @param fromhost canonical host name of the machine hosting the bag
  * @param scheme designates a file system provider for accessing this replica
  * @param authority remote connection information for network replicas,
  * or <code>null</code> for local file system
  * @param path canonical path to the replica's location
  * @return object that represents the requested replica record
  * or <code>null</code> if there is no match
  * @throws DBException if there is an error accessing the database
  */
 public ReplicaDTO findReplica(String foruser, String fromhost,
   String scheme, String authority, String path)
 	throws DBException
 {
  Loader loader = new Loader(mgr, foruser, fromhost, scheme, authority, path);
  loader.execute();
  return loader.getReplica();
 }

 /**
  * Looks up replica information for the id.
  * @param id internal identifier of a replica's record in the database
  * @return object that represents the requested replica record
  * or <code>null</code> if there is no match
  * @throws DBException if there is an error accessing the database
  */
 public ReplicaDTO findReplica(Number id)
 	throws DBException
 {
  Loader loader = new Loader(mgr, id);
  loader.execute();
  return loader.getReplica();
 }

 /**
  * Designates a replica as a default replica for its owning user.
  * There may be at most one default replica for each host/user/scheme/authority
  * combination. 
  * @param id identifier of the replica the replica to designate as default
  * @throws DBException if there is an error accessing the database
  */
 public void setDefaultReplica(Number id)
	throws DBException
 {
  final ReplicaDTO replica = findReplica(id);
  if (null == replica)
   throw new NoSuchRecordException(TABLE_NAME, id);
  final String legend = "designating " + replica + " as the default replica";
  class Updater extends PreparedStatementHandler
  {
   public boolean isComplete()
   {
    return complete;
   }

   public Updater()
   {
    super(ReplicaDAO.this.mgr, UPDATE_DEFAULT_SQL);
   }

   @Override
   protected void bindParameters(PreparedStatement stmt) throws SQLException
   {
    stmt.setInt(1, replica.getId());
    stmt.setString(2, replica.getForUser());
    stmt.setString(3, replica.getFromHost());
    stmt.setString(4, replica.getScheme());
    stmt.setString(5, replica.getAuthority());
   }

   @Override
   protected void handleUpdate(int count) throws DBException
   {
    if (0 < count)
     complete = true;
   }

   @Override
   protected String legend()
   {
    return legend;
   }

   private boolean complete;
  };

  Updater phase1 = new Updater();
  phase1.execute();
  if (!phase1.isComplete())
   new PreparedStatementHandler(mgr, INSERT_DEFAULT_SQL)
   {
    @Override
    protected void bindParameters(PreparedStatement stmt) throws SQLException
    {
     stmt.setInt(1, replica.getId());
     stmt.setString(2, replica.getForUser());
     stmt.setString(3, replica.getFromHost());
     stmt.setString(4, replica.getScheme());
     stmt.setString(5, replica.getAuthority());
    }

    @Override
    protected void noMatchOnUpdate() throws DBException
    {
     throw new DBException("Could not insert default marker for " + replica);
    }

    @Override
    protected String legend()
    {
     return legend;
    }
   }.execute();
 }

 /**
  * Lists known replica records for a specific user.
  * @param foruser user name on the machine hosting the bag
  * @param fromhost host name of the machine hosting the bag
  * @param withViews asks to include derived replicas (views) in the list
  * @return cursor over replica objects
  * @throws DBException if there is an error accessing the database
  */
 public Cursor<ReplicaDTO> listReplicas(final String foruser,
   final String fromhost, final boolean withViews)
	throws DBException
 {
  PreparedStatementCursor<ReplicaDTO> cursor = 
   new PreparedStatementCursor<ReplicaDTO>(mgr,
     withViews ? LIST_USERHOST_SQL : LIST_USERHOST_NOVIEWS_SQL)
   {
    @Override
    protected void bindParameters(PreparedStatement stmt) throws SQLException
    {
     stmt.setString(1, foruser);
     stmt.setString(2, fromhost);
    }

    @Override
    protected ReplicaDTO loadInstance(ResultSet rs) throws SQLException
    {
     ReplicaDTO replica = new ReplicaDTO();
     replica.load(rs);
     return replica;
    }

    @Override
    protected String legend()
    {
     return "listing replicas for " + foruser + '@' + fromhost;
    }
   };
  try
  {
   cursor.execute();
   return cursor;
  }
  catch (Exception e)
  {
   cursor.close();
   if (e instanceof DBException)
    throw (DBException)e;
   else
    throw (RuntimeException)e;
  }
 }

 /**
  * Saves changes to a replica object.
  */
 public void update(final ReplicaDTO replica)
	throws DBException
 {
  new PreparedStatementHandler(mgr, UPDATE_SQL) {
   @Override
   protected void bindParameters(PreparedStatement stmt) throws SQLException
   {
    replica.bindCommonFields(stmt);
    stmt.setInt(9, replica.getId());
   }

   @Override
   protected void noMatchOnUpdate() throws DBException
   {
    throw new NoSuchRecordException("Replica", replica.toString());
   }

   @Override
   protected String legend()
   {
    return "saving changes to " + replica;
   }
  }.execute();
 }

 /**
  * Insert new replica record into the database.
  * @param replica object that represents the new replica record
  * @throws DBException if there is an error updating the database
  */
 public void insert(final ReplicaDTO replica)
 	throws DBException
 {
  replica.setId(0);
  new PreparedStatementHandler(mgr, INSERT_SQL) {
   @Override
   protected void bindParameters(PreparedStatement stmt) throws SQLException
   {
    replica.bindCommonFields(stmt);
   }

   @Override
   protected void handleUpdate(PreparedStatement stmt)
   	throws DBException, SQLException
   {
    if (0 < stmt.getUpdateCount())
    {
     ResultSet idrs = stmt.getGeneratedKeys();
     if (idrs.next())
      replica.setId(idrs.getInt(1));
    }
    if (0 == replica.getId())
     throw new DBException("No record has been added for " + replica);
   }

   @Override
   protected String legend()
   {
    return "adding " + replica;
   }
  }.execute();
 }

 /**
  * Delete a replica record from the database.
  * @param id identity of the replica to be deleted
  * @throws DBException if there is an error updating the database
  */
 public void delete(final Number id)
 	throws DBException
 {
  new PreparedStatementHandler(mgr, DELETE_SQL) {
   @Override
   protected void bindParameters(PreparedStatement stmt) throws SQLException
   {
    stmt.setInt(1, id.intValue());
   }

   @Override
   protected void noMatchOnUpdate() throws DBException
   {
    throw new NoSuchRecordException(TABLE_NAME, id);
   }

   @Override
   protected String legend()
   {
    return "deleting replica #" + id;
   }
  }.execute();
 }

 /**
  * Counts replicas that use a specific filter as the default.
  * May not return a correct count of replicas that use the "all" filter.
  * @param filter in-memory filter object, must have both
  * {@link FilterDTO#getId() id} and {@link FilterDTO#getName() name}
  * properties initialized, load an object from the database to
  * be sure that's the case 
  */ // Q18REP06
 public int countReplicasWithFilter(final FilterDTO filter)
 	throws DBException
 {
  class Counter extends PreparedStatementHandler
  {
   Counter()
   {
    super(ReplicaDAO.this.mgr, COUNT_FILTER_USE_SQL);
   }

   public void setFilterId(Integer filterId)
   {
    this.filterId = filterId;
    sql = null == filterId ? COUNT_DEFAULT_FILTER_USE_SQL : COUNT_FILTER_USE_SQL;
   }

   public int getCount()
   {
    return count;
   }

   @Override
   protected String legend()
   {
    return "counting replicas using " + 
    	(COUNT_FILTER_USE_SQL == sql ? "filter \"" + filter.getName() + '"'
    	  : "the global default")
    	+ " as the default filter";
   }

   @Override
   protected void bindParameters(PreparedStatement stmt) throws SQLException
   {
    count = -1;
    if (null != filterId)
     stmt.setInt(1, filterId);
   }

   @Override
   protected void handleResults(ResultSet rs) throws SQLException, DBException
   {
    if (!rs.next())
     throw new DBException("Query returned no results while " + legend());
    count = rs.getInt(1);
   }

   private Integer filterId;
   private int count = -1;
  }

  int id = filter.getId();
  if (0 >= id)
   throw new IllegalArgumentException("Filter id was not initialized");
  Counter counter = new Counter();
  counter.setFilterId(id);
  counter.execute();
  int count = counter.getCount();
  if (FilterDAO.DEFAULT_FILTER.equalsIgnoreCase(filter.getName()))
  {
   counter.setFilterId(null);
   counter.execute();
   count += counter.getCount();
  }
  return count;
 }

 @Override
 public int getCurrentVersion()
 {
  return SCHEMA_VERSION;
 }

 @Override
 public int getOldestUpgradableVersion()
 {
  return getUpgradeScripts().getOldestUpgradableVersion();
 }

 @SuppressWarnings("unchecked")
 @Override
 public Class<? extends AbstractDAO>[] dependencies()
 {
  return (Class<? extends AbstractDAO>[])DEPENDENCIES;
 }

 /* (non-Javadoc)
  * @see name.livitski.databag.db.AbstractDAO#schemaDDL()
  */
 @Override
 public String[] schemaDDL()
 {
  return DDL_V5;
 }

 public static final String TABLE_NAME = "Replica";
 public static final String DEFAULTS_TABLE_NAME = "ReplicaDefault";

 @Override
 protected int upgradeSchema(int dbVersion)
 	throws DBException, IncompatibleSchemaException
 {
  return getUpgradeScripts().upgradeSchema(dbVersion);
 }

 protected SchemaUpgrades getUpgradeScripts()
 {
  if (null == upgrades)
   upgrades = new SchemaUpgrades(this, MIGRATE_SCRIPTS, SCHEMA_VERSION);
  return upgrades;
 }

 /**
  * Creates a DAO object as specified by the superclass. The constructor need not
  * be public as only the {@link Manager database manager} may instantiate this object. 
  * @param mgr database manager reference
  */
 protected ReplicaDAO(Manager mgr)
 {
  super(mgr);
 }

 protected static class Loader extends PreparedStatementHandler
 {
  public ReplicaDTO getReplica()
  {
   return replica;
  }
  
  public Loader(Manager mgr, Number id)
  {
   super(mgr, LOAD_BYID_SQL);
   this.id = id.intValue();
  }

  public Loader(Manager mgr, String foruser, String fromhost,
    String scheme, String authority)
  {
   super(mgr, LOAD_DEFAULT_SQL);
   this.foruser = foruser;
   this.fromhost = fromhost;
   this.scheme = scheme;
   this.authority = authority;
  }
  
  public Loader(Manager mgr, String foruser, String fromhost,
    String scheme, String authority, String path)
  {
   super(mgr, LOAD_SQL);
   this.foruser = foruser;
   this.fromhost = fromhost;
   this.path = path;
   this.scheme = scheme;
   this.authority = authority;
  }
 
  @Override
  protected void bindParameters(PreparedStatement stmt)
        throws SQLException
  {
   if (LOAD_BYID_SQL == sql)
    stmt.setInt(1, id);
   else
   {
    stmt.setString(1, foruser);
    stmt.setString(2, fromhost);
    stmt.setString(3, scheme);
    stmt.setString(4, authority);
    if (LOAD_SQL == sql)
     stmt.setString(5, path);
   }
  }
 
  @Override
  protected void handleResults(ResultSet rs)
  	throws SQLException, DBException
  {
   if (!rs.next())
   {
    replica = null;
    if (sql == LOAD_DEFAULT_SQL)
    {
     sql = LOAD_FIRST_SQL;
     execute();
    }
   }
   else
   {
    replica = new ReplicaDTO();
    replica.load(rs);
   }
  }
 
  /* (non-Javadoc)
   * @see name.livitski.databag.db.StatementHandler#legend()
   */
  @Override
  protected String legend()
  {
   return "retrieving replica information for " + foruser + '@' + fromhost;
  }

  private int id;
  private ReplicaDTO replica;
  private String foruser, fromhost;
  private String scheme, authority, path;
 }

 protected static Class<?>[] DEPENDENCIES = {
  	FilterDAO.class
 };
 
 protected static final String COMMON_FIELDS
  = "foruser,fromhost,scheme,authority,path,viewof,askpassword,default_filter";
 
 /**
  * Version 5 DDL of the ReplicaDefault table.
  */
 protected static final String DDL_DEFAULTS_V5 =
   "CREATE TABLE " + DEFAULTS_TABLE_NAME + " (" +
     "fromhost VARCHAR(255)," +
     "foruser VARCHAR(256)," +
     "scheme VARCHAR(255) NOT NULL," +
     "authority VARCHAR(1024) NOT NULL," + 
     "default INTEGER NOT NULL," +
     "PRIMARY KEY (fromhost, foruser, scheme, authority)," +
     "CONSTRAINT FK_ReplicaDefault_default" +
     " FOREIGN KEY (default, fromhost, foruser, scheme, authority)" +
     " REFERENCES " + TABLE_NAME + 
     "(id, fromhost, foruser, scheme, authority) ON DELETE CASCADE ON UPDATE CASCADE)"; 

 protected static final String CONSTRAINT_FK_DEFAULT_FILTER =
   "CONSTRAINT FK_Replica_default_filter" +
     " FOREIGN KEY (default_filter) REFERENCES Filter ON DELETE SET NULL";

 /**
  * Version 5 DDL of the Replica table.
  */
 protected static final String[] DDL_V5 = {
  "CREATE TABLE " + TABLE_NAME + "(" +
  " id INTEGER IDENTITY," +
  " fromhost VARCHAR(255)," +
  " foruser VARCHAR(256)," +
  " scheme VARCHAR(255) NOT NULL," +
  " authority VARCHAR(1024) DEFAULT '' NOT NULL," + 
  " path VARCHAR(16384) NOT NULL," +
  " viewof INTEGER," + 
  " askpassword BOOLEAN DEFAULT FALSE NOT NULL," + 
  " default_filter INTEGER," +
  " CONSTRAINT CU_Replica_location UNIQUE (fromhost, foruser, scheme, authority, path)," + 
  " CONSTRAINT FK_View FOREIGN KEY (viewof) REFERENCES " + TABLE_NAME
  + " ON DELETE RESTRICT ON UPDATE CASCADE," +
  CONSTRAINT_FK_DEFAULT_FILTER +
  ")",
  DDL_DEFAULTS_V5
 };
 
 /**
  * Script for migrating the schema for version 4 to version 5.
  */
 protected static final Object[] MIGRATE_SQL_V4 = {
   "UPDATE " + TABLE_NAME + " SET authority='' WHERE authority IS NULL",
   "ALTER TABLE " + TABLE_NAME + " ALTER COLUMN authority VARCHAR(1024) DEFAULT '' NOT NULL",
   "ALTER TABLE " + TABLE_NAME + " DROP CONSTRAINT CU_Replica_location",
   "ALTER TABLE " + TABLE_NAME + " ADD CONSTRAINT CU_Replica_location"
     + " UNIQUE (fromhost, foruser, scheme, authority, path)",
   
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " DROP PRIMARY KEY",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ADD scheme VARCHAR(255) BEFORE default",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ADD authority VARCHAR(1024) BEFORE default",
   "UPDATE " + DEFAULTS_TABLE_NAME + " d SET scheme=(SELECT scheme FROM "
     + TABLE_NAME + " r WHERE r.id = d.default) WHERE scheme IS NULL",
   "UPDATE " + DEFAULTS_TABLE_NAME + " d SET authority=(SELECT authority FROM "
     + TABLE_NAME + " r WHERE r.id = d.default) WHERE authority IS NULL",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ALTER COLUMN scheme VARCHAR(255) NOT NULL",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ALTER COLUMN authority VARCHAR(1024) NOT NULL",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ADD PRIMARY KEY"
     + " (fromhost, foruser, scheme, authority)",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " DROP CONSTRAINT FK_ReplicaDefault_default",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ADD CONSTRAINT FK_ReplicaDefault_default" +
     " FOREIGN KEY (default, fromhost, foruser, scheme, authority)" +
     " REFERENCES " + TABLE_NAME + 
     "(id, fromhost, foruser, scheme, authority) ON DELETE CASCADE ON UPDATE CASCADE"
 };
 
 /**
  * Version 4 DDL of the ReplicaDefault table.
  */
 protected static final String DDL_DEFAULTS_V4 =
  "CREATE TABLE " + DEFAULTS_TABLE_NAME + " (" +
  "fromhost VARCHAR(255)," +
  "foruser VARCHAR(256)," +
  "default INTEGER NOT NULL," +
  "PRIMARY KEY (fromhost, foruser)," +
  "CONSTRAINT FK_ReplicaDefault_default FOREIGN KEY (default, fromhost, foruser)" +
  " REFERENCES " + TABLE_NAME + "(id, fromhost, foruser) ON DELETE CASCADE ON UPDATE CASCADE)"; 

 protected static final String[] DDL_V4 = {
   "CREATE TABLE " + TABLE_NAME + "(" +
   " id INTEGER IDENTITY," +
   " fromhost VARCHAR(255)," +
   " foruser VARCHAR(256)," +
   " scheme VARCHAR(255) NOT NULL," +
   " authority VARCHAR(1024)," + 
   " path VARCHAR(16384) NOT NULL," +
   " viewof INTEGER," + 
   " askpassword BOOLEAN DEFAULT FALSE NOT NULL," + 
   " default_filter INTEGER," +
   " CONSTRAINT CU_Replica_location UNIQUE (fromhost, foruser, path)," + 
   " CONSTRAINT FK_View FOREIGN KEY (viewof) REFERENCES " + TABLE_NAME
   + " ON DELETE RESTRICT ON UPDATE CASCADE," +
   CONSTRAINT_FK_DEFAULT_FILTER +
   ")",
   DDL_DEFAULTS_V4
  };

 /**
  * Script for migrating the schema for version 3 to version 4.
  */
 protected static final Object[] MIGRATE_SQL_V3 = {
   "ALTER TABLE " + TABLE_NAME + " ALTER COLUMN host_v2 RENAME TO fromhost",
   "ALTER TABLE " + TABLE_NAME + " ALTER COLUMN user RENAME TO foruser",
   "ALTER TABLE " + TABLE_NAME + " ADD "
     + "scheme VARCHAR(255) DEFAULT '" + ReplicaDTO.DEFAULT_SCHEME
     + "' NOT NULL BEFORE path",
   "ALTER TABLE " + TABLE_NAME + " ALTER COLUMN "
     + "scheme VARCHAR(255) NOT NULL",
   "ALTER TABLE " + TABLE_NAME + " ADD "
     + "authority VARCHAR(1024) BEFORE path",
   "ALTER TABLE " + TABLE_NAME + " ADD "
     + "viewof INTEGER BEFORE default_filter",
   "ALTER TABLE " + TABLE_NAME + " ADD "
     + "askpassword BOOLEAN DEFAULT FALSE NOT NULL BEFORE default_filter",
   "ALTER TABLE " + TABLE_NAME + " ADD CONSTRAINT "
     + "FK_View FOREIGN KEY (viewof) REFERENCES " + TABLE_NAME
     + " ON DELETE RESTRICT ON UPDATE CASCADE",
     
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ALTER COLUMN host_v2 RENAME TO fromhost",
   "ALTER TABLE " + DEFAULTS_TABLE_NAME + " ALTER COLUMN user RENAME TO foruser",
 };
 
 /**
  * Version 2 DDL of the ReplicaDefault table.
  */
 protected static final String DDL_DEFAULTS_V2 =
  "CREATE TABLE " + DEFAULTS_TABLE_NAME + " (" +
  "host_v2 VARCHAR(255)," +
  "user VARCHAR(256)," +
  "default INTEGER NOT NULL," +
  "PRIMARY KEY (host_v2, user)," +
  "CONSTRAINT FK_ReplicaDefault_default FOREIGN KEY (default, host_v2, user)" +
  " REFERENCES " + TABLE_NAME + "(id, host_v2, user) ON DELETE CASCADE ON UPDATE CASCADE)"; 

 /**
  * Version 3 DDL of the Replica table.
  */
 protected static final String[] DDL_V3 = {
  "CREATE TABLE " + TABLE_NAME + "(" +
  " id INTEGER IDENTITY," +
  " host_v2 VARCHAR(255)," +
  " user VARCHAR(256)," +
  " path VARCHAR(16384) NOT NULL," +
  " default_filter INTEGER," +
  " CONSTRAINT CU_Replica_location UNIQUE (host_v2, user, path)," +
  CONSTRAINT_FK_DEFAULT_FILTER +
  ")",
  DDL_DEFAULTS_V2
 };

 protected static final String V2_SYNC_MIGRATION_TABLE_DDL =
  "CREATE TABLE IF NOT EXISTS Replica_v2_synced(" + 
  " replica INTEGER NOT NULL UNIQUE REFERENCES " + TABLE_NAME + " ON DELETE CASCADE," + 
  " synced TIMESTAMP NOT NULL" + 
  ")";

 /**
  * Script for migrating the schema for version 2 to version 3.
  */
 protected static final Object[] MIGRATE_SQL_V2 = {
  V2_SYNC_MIGRATION_TABLE_DDL,
  "INSERT INTO Replica_v2_synced(replica, synced)" +
  " SELECT id, synced FROM " + TABLE_NAME + " WHERE synced IS NOT NULL",
  "ALTER TABLE " + TABLE_NAME + " DROP COLUMN synced",
  "ALTER TABLE " + TABLE_NAME + " ADD default_filter INTEGER",
  "ALTER TABLE " + TABLE_NAME + " ADD " + CONSTRAINT_FK_DEFAULT_FILTER + " NOCHECK"
 };

 /**
  * Version 2 DDL of the Replica table.
  */
 protected static final String[] DDL_V2 = {
  "CREATE TABLE " + TABLE_NAME + " (" +
    "id INTEGER IDENTITY," +
    "host_v2 VARCHAR(255)," + // prevent corruption by v 0.01
    "user VARCHAR(256)," +
    "path VARCHAR(16384) NOT NULL," +
    "synced TIMESTAMP," +
    "CONSTRAINT CU_Replica_location UNIQUE (host_v2, user, path))",
    DDL_DEFAULTS_V2
 };

 /**
  * Script for migrating the schema for version 1 to version 2.
  */
 protected static final Object[] MIGRATE_SQL_V1 = {
  "ALTER TABLE " + TABLE_NAME + " RENAME TO Replica_v1",
  DDL_V2,
  "INSERT INTO " + TABLE_NAME + " (user, host_v2, path, synced)" +
  " SELECT user, host, directory, synced FROM Replica_v1",
  "DROP TABLE Replica_v1"
 };

 /**
   * Version 1 DDL of the Replica table.
   *
  protected static final String DDL = "CREATE TABLE " + TABLE_NAME +
    "( host VARCHAR(255)" +
    ", user VARCHAR(256)" +
    ", directory VARCHAR(16384) NOT NULL" +
    ", synced TIMESTAMP" +
    ", PRIMARY KEY (host, user))";
 */

 protected static final int SCHEMA_VERSION = 5;

 protected static final Object[][] MIGRATE_SCRIPTS = {
  	MIGRATE_SQL_V1,
  	MIGRATE_SQL_V2,
  	MIGRATE_SQL_V3,
  	MIGRATE_SQL_V4
 };

 /**
  * SQL statement for listing replicas for a user.
  */
 protected static final String LIST_USERHOST_SQL =
  "SELECT id, " + COMMON_FIELDS + " FROM " + TABLE_NAME +
  " WHERE foruser = ? AND fromhost = ?";

 /**
  * SQL statement for listing replicas for a user, except the derived replicas.
  */
 protected static final String LIST_USERHOST_NOVIEWS_SQL =
   LIST_USERHOST_SQL + " AND viewof IS NULL";

 /**
  * SQL statement for loading replica objects.
  */
 protected static final String LOAD_SQL =
  "SELECT id, " + COMMON_FIELDS + " FROM " + TABLE_NAME +
  " WHERE foruser = ? AND fromhost = ? AND scheme = ? AND authority = ? AND path = ?";

 /**
  * SQL statement for loading the default replica, if defined.
  */
 protected static final String LOAD_DEFAULT_SQL =
  "SELECT r.id, r.foruser, r.fromhost, r.scheme, r.authority, r.path, r.viewof,"
  + " r.askpassword, r.default_filter" +
  " FROM " + TABLE_NAME + " r JOIN " + DEFAULTS_TABLE_NAME + " d ON r.id=d.default" +
  " WHERE d.foruser = ? AND d.fromhost = ? AND d.scheme = ? " +
  " AND d.authority = ?";

 /**
  * SQL statement for loading a replica by its internal identifier.
  */
 protected static final String LOAD_BYID_SQL = 
  "SELECT id, " + COMMON_FIELDS + " FROM " + TABLE_NAME +
  " WHERE id = ?";
 
 /**
  * SQL statement for choosing the default replica if none is defined.
  */
 protected static final String LOAD_FIRST_SQL =
  "SELECT id, " + COMMON_FIELDS + " FROM " + TABLE_NAME +
  " WHERE id = (SELECT MIN(id) FROM " + TABLE_NAME + 
  " WHERE foruser = ? AND fromhost = ? AND scheme = ? AND authority = ?)";

 /**
  * SQL statement for updating replica objects.
  */
 protected static final String UPDATE_SQL =
  "UPDATE " + TABLE_NAME + " SET foruser = ?, fromhost = ?, "
    + "scheme = ?, authority = ?, path = ?, viewof = ?, askpassword = ?, "
    + "default_filter = ? WHERE id = ?";

 /**
  * SQL statement for inserting replica objects.
  */
 protected static final String INSERT_SQL =
  "INSERT INTO " + TABLE_NAME + " (" + COMMON_FIELDS + ") VALUES (?,?,?,?,?,?,?,?)";

 /**
  * SQL statement for changing a default replica.
  * Parameters are: path, foruser, fromhost, scheme, authority.
  */
 protected static final String UPDATE_DEFAULT_SQL =
  "UPDATE " + DEFAULTS_TABLE_NAME + " d SET default = ?"
    + " WHERE d.foruser = ? AND d.fromhost = ? AND d.scheme = ?"
    + " AND d.authority = ?";

 /**
  * SQL statement for designating a new default replica.
  * Parameters are: path, foruser, fromhost, scheme, authority.
  */
 protected static final String INSERT_DEFAULT_SQL =
  "INSERT INTO " + DEFAULTS_TABLE_NAME +
  " (default, foruser, fromhost, scheme, authority) VALUES (?,?,?,?,?)";

 /**
  * SQL statement for deleting replica objects.
  */
 protected static final String DELETE_SQL = "DELETE FROM " + TABLE_NAME + " WHERE id = ?";

 /**
  * SQL statement for counting replica objects using a specific filter by default.
  */
 protected static final String COUNT_FILTER_USE_SQL =
  "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE default_filter = ?";

 /**
  * SQL statement for counting replica objects using the default filter.
  */
 protected static final String COUNT_DEFAULT_FILTER_USE_SQL =
  "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE default_filter IS NULL";

 private SchemaUpgrades upgrades;
}
