/**
 *  Copyright 2010-2013, 2023 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.db.schema;

import java.nio.file.FileSystems;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * In-memory representation of a replica's information from the database. 
 */
public class ReplicaDTO
{
 public String getForUser()
 {
  return forUser;
 }

 public void setForUser(String user)
 {
  this.forUser = user;
 }

 public String getFromHost()
 {
  return fromHost;
 }

 public void setFromHost(String host)
 {
  this.fromHost = host;
 }

 /**
  * Designates a file system provider for accessing this replica.
  * @see java.nio.file.spi.FileSystemProvider#getScheme
  */
 public String getScheme()
 {
  return scheme;
 }

 public void setScheme(String scheme)
 {
  this.scheme = scheme;
 }

 /**
  * Remote connection information for network replicas,
  * or an empty string for replicas on the local file system.
  */
 public String getAuthority()
 {
  return authority;
 }

 public void setAuthority(String authority)
 {
  this.authority = authority;
 }

 /**
  * This field SHOULD contain canonical path to the replica.
  * With databases upgraded from v0.01, paths may be absolute,
  * but non-canonical.
  */
 public String getPath()
 {
  return path;
 }

 /**
  * @see #getPath
  */
 public void setPath(String path)
 {
  this.path = path;
 }

 /**
  * Whether or not a service working with this replica needs
  * to ask for password and store it in memory while it performs
  * an operation. Services <em>SHOULD NOT</em> store passwords
  * in a database as that creates a security risk.
  */
 public boolean isAskingPassword()
 {
  return askingPassword;
 }

 /**
  * @see #isAskingPassword
  */
 public void setAskingPassword(boolean askingPassword)
 {
  this.askingPassword = askingPassword;
 }

 public Integer getViewOf()
 {
  return 0 == viewOf ? null : viewOf;
 }

 public void setViewOf(Integer viewOf)
 {
  this.viewOf = null == viewOf ? 0 : viewOf.intValue();
 }

 public Number getDefaultFilterId()
 {
  return 0 == defaultFilterId ? null : defaultFilterId;
 }

 public void setDefaultFilterId(Number defaultFilterId)
 {
  this.defaultFilterId = null == defaultFilterId ? 0 : defaultFilterId.intValue();
 }

 public int getId()
 {
  return id;
 }

 @Override
 public String toString()
 {
  return (0 == id ? "new replica" : "replica #" + id)
  	+ " for " + forUser + '@' + fromHost
  	+ (0 == viewOf ? "" : ", mirroring replica #" + viewOf)
  	+ " at " + scheme + ":"
  	+ (null == authority ? "" : "//" + authority) + path;
 }

 public ReplicaDTO()
 {
 }

 public static final String DEFAULT_SCHEME =
   FileSystems.getDefault().provider().getScheme();

 protected void setId(int id)
 {
  this.id = id;
 }

 protected void bindCommonFields(PreparedStatement stmt) throws SQLException
 {
  stmt.setString(1, forUser);
  stmt.setString(2, fromHost);
  stmt.setString(3, scheme);
  stmt.setString(4, authority);
  stmt.setString(5, path);
  if (0 == viewOf)
   stmt.setNull(6, Types.INTEGER);
  else
   stmt.setInt(6, viewOf);
  stmt.setBoolean(7, askingPassword);
  if (0 == defaultFilterId)
   stmt.setNull(8, Types.INTEGER);
  else
   stmt.setInt(8, defaultFilterId);
 }

 protected void load(ResultSet rs) throws SQLException
 {
  id = rs.getInt(1);
  forUser = rs.getString(2);
  fromHost = rs.getString(3);
  scheme = rs.getString(4);
  authority = rs.getString(5);
  path = rs.getString(6);
  viewOf = rs.getInt(7);
  askingPassword = rs.getBoolean(8);
  defaultFilterId = rs.getInt(9);
 }

 private String forUser, fromHost;
 private String scheme = DEFAULT_SCHEME;
 private String authority = "";
 private String path;
 private boolean askingPassword;
 private int id, viewOf, defaultFilterId;
}
