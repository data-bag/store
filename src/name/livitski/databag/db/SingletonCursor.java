/**
 *  Copyright 2010-2013 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.db;

/**
 * A cursor that returns a single data item.
 */
public class SingletonCursor<T> implements Cursor<T>
{
 public void close()
 {
  this.item = null;
 }

 public T next()
 {
  T item = this.item;
  this.item = null;
  return item;
 }

 public SingletonCursor(T item)
 {
  this.item = item;
 }

 private T item;
}
