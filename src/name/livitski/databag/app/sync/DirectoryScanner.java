/**
 *  Copyright 2010-2014, 2023 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.app.sync;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import name.livitski.databag.app.filter.PathFilter;
import name.livitski.databag.app.filter.PathMatcher;

/**
 * Scans a directory recursively to find all files
 * that match a {@link PathFilter filter}.
 */
public class DirectoryScanner
{
 /**
  * Scans the {@link #DirectoryScanner(File) root directory} and
  * its descendants and collects all files that match the
  * {@link #setFilter(PathFilter) filter}. If there is no filter,
  * all descendant files are collected. Results of the previous scan,
  * if any, are discarded. Call {@link #getFiles()} to retrieve the
  * results.
  * @throws IOException if the replica's root is not a directory, or if
  * it doesn't exist and cannot be created 
  */
 public void scan()
 	throws IOException
 {
  paths = new LinkedHashSet<Path>();
  ancestorPaths = new HashSet<Path>();
  splitPath = new ArrayList<String>();
  scanPaths(null);
 }

 /**
  * Returns relative paths to files found as the result of a last
  * {@link #scan()} operation. Returned paths are relative to the
  * {@link #DirectoryScanner(Path) root directory} of this scanner
  * and belong to its file system.
  * @return the set of file locations or <code>null</code> if no scan
  * has been done since the object's creation or last
  * {@link #setFilter filter assignment} 
  */
 public Set<Path> getFiles()
 {
  return paths;
 }

 /**
  * Assigns a filter to be used when {@link #scan() scanning}
  * a directory. You must do a re-scan after assigning a filter. 
  * @param filter the new filter <code>null</code> to match
  * all files that match the {@link #setPattern(PathMatcher) pattern}
  */
 public void setFilter(PathFilter filter)
 {
  this.filter = filter;
  this.paths = null;
 }

 /**
  * Assigns a pattern to match relative paths against when
  * {@link #scan() scanning} a directory. You must do a re-scan
  * after changing the pattern. 
  * @param pattern the new pattern or <code>null</code> to match
  * all files that pass the {@link #setFilter(PathFilter) filter}
  */ 
 public void setPattern(PathMatcher pattern)
 {
  this.pattern = pattern;
  this.paths = null;
 }

 /**
  * Creates a scanner that enumerates descendant files
  * of a directory. 
  * @param root the root directory of a replica to scan or
  * <code>null</code> to create a dummy scanner that never
  * finds any files
  */
 public DirectoryScanner(Path root)
 {
  this.root = root;
 }

 /**
  * Used by {@link #scan()} to enumerate files. Avoids endless loops
  * in the presence of links to ancestor directories.
  */
 protected void scanPaths(Path parent)
	throws IOException
 {
  Path dir = null == parent ? root : root.resolve(parent);
  if (null == dir || !Files.isDirectory(dir))
   return;
  // Prevent endless recursion due to symlinks
  Path canonical = dir.toRealPath();
  if (!ancestorPaths.add(canonical))
  	return;
  try (Stream<Path> list = Files.list(dir))
  {
   final Iterator<Path> i = list.iterator();
   while (i.hasNext())
   {
    Path path = i.next();
    splitPath.add(path.getFileName().toString());
    if (Files.isDirectory(path))
     scanPaths(path);
    else
    {
     String[] splitPathArray = splitPath.toArray(DUMMY_ARRAY);
     if ((null == pattern || pattern.pathMatches(splitPathArray))
       && (null == filter || filter.pathMatches(splitPathArray)))
      paths.add(root.relativize(path));
    }
    splitPath.remove(splitPath.size() - 1);
   }
  }
  ancestorPaths.remove(canonical);
 }

 private Path root;
 private PathFilter filter;
 private PathMatcher pattern;
 private Set<Path> paths;
 private Set<Path> ancestorPaths;
 private List<String> splitPath;
 private static String[] DUMMY_ARRAY = {};
}
