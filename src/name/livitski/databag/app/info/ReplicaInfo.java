/**
 *  Copyright 2010-2013, 2023 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.app.info;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import name.livitski.databag.app.ConfigurableService;
import name.livitski.databag.app.filter.PathMatcher;
import name.livitski.databag.db.DBException;
import name.livitski.databag.db.Function;
import name.livitski.databag.db.schema.ReplicaDTO;

/**
 * Provides information about a local copy of files on the shared
 * medium (a replica).
 */
public class ReplicaInfo
{
 public Number getDefaultFilterId()
 {
  return data.getDefaultFilterId();
 }

 public String getHost()
 {
  return data.getFromHost();
 }

 public String getUser()
 {
  return data.getForUser();
 }

 /**
  * @return unique record identifier of this replica's record
  * @see name.livitski.databag.db.schema.ReplicaDTO#getId()
  */
 public Number getId()
 {
  return data.getId();
 }

 /**
  * Returns the path to this replica's root directory.
  * @throws IOException if there's an error accessing target filesystem
  */
 public Path rootPath() throws IOException
 {
  return ConfigurableService.getReplicaPath(data);
 }

 /**
  * Checks case-sensitivity of the file names in the file system hosting this
  * replica.
  * @return whether or not the current replica is case-sensitive
  * @throws IOException if the check fails
  */
 public boolean checkCaseSensitivity()
   throws IOException
 {
  Path root = rootPath();
  if (!Files.exists(root))
  {
   root = root.getParent();
   if (null == root || !Files.exists(root))
    throw new IllegalArgumentException(
      "Replica's root path '" + rootPath()
      + "' is not contained in an existing directory");
  }
  return PathMatcher.checkFSCaseSensitivity(root);
 }

 @Override
 public String toString()
 {
  return "info wrapper for " + data.toString();
 }

 protected ReplicaInfo(ReplicaDTO data)
 {
  this.data = data;
 }

 protected ReplicaDTO getData()
 {
  return data;
 }

 protected static class ReplicaDTOConverter implements Function<ReplicaDTO, ReplicaInfo>
 {
  public ReplicaInfo exec(ReplicaDTO record) throws DBException
  {
   return new ReplicaInfo(record);
  }
 }

 private ReplicaDTO data;
}
