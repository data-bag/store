/**
 *  Copyright 2010-2013 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.diff;

/**
 * Provides a collection of deltas sufficient for
 * a {@link Restorer} to build modified image from
 * the original image.
 */
public interface EffectiveDelta
{
 public CommonDeltaSource getCommonDelta();
 public DirectionalDeltaSource getDirectionalDelta();
}
