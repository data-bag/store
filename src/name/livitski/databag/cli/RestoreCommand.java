/**
 *  Copyright 2010-2013, 2023 Stan Livitski
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the Data-bag Project License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  Data-bag Project License for more details.
 *
 *  You should find a copy of the Data-bag Project License in the
 *  `data-bag.md` file in the `LICENSE` directory
 *  of this package or repository. 
 */
    
package name.livitski.databag.cli;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.logging.Level;

import name.livitski.databag.app.Configuration;
import name.livitski.databag.app.filter.PathMatcher;
import name.livitski.databag.app.info.ReplicaInfo;
import name.livitski.databag.app.sync.RestoreService;
import name.livitski.databag.db.DBException;
import name.livitski.databag.db.Manager;

/**
 * Implements the {@link Syntax#RESTORE_COMMAND} restore command.
 */
public class RestoreCommand extends PointInTimeAbstractCommand
{
 @Override
 protected void runProtected() throws Exception
 {
  Path output = getOutputDir();
  if (null == output && null == getCurrentReplica())
   throw new IllegalArgumentException(
     "The location to restore files is unknown and no replica is currently available.");
  String nameOption = getNameOption();
  if (null != nameOption && PathMatcher.hasWildcards(nameOption))
   restoreMatchingFiles(nameOption, output);
  else
   processSingleFile();
 }

 protected void restoreMatchingFiles(String patternString, Path target)
 	throws Exception
 {
  blockFileAndVersionIds("restoring multiple files");
  if (null == target)
  {
   if (null != getOutputStream())
    throw new IllegalArgumentException(
      "Cannot restore multiple files to an output stream or pipe.");
  }
  else
  {
   ReplicaInfo replica = getCurrentReplica();
   if (!Files.isDirectory(target))
    throw new IllegalArgumentException("The output path '" + target
	+ "' must point to an existing directory to restore multiple files.");
   else if (null != replica)
   {
    Path replicaRoot = replica.rootPath();
    if (target.toRealPath().equals(replicaRoot))
     throw new IllegalArgumentException("The output path '" + target
	+ "' points to the current replica's root directory. Please omit the --"
	+ Syntax.SAVE_OPTION + " option to restore multiple files into the current replica."
	+ " Note the different file replacement rules that apply to in-place restore.");
   }
  }
  final PathMatcher namePattern = new PathMatcher(
    patternString,
    null == target ?
      checkReplicasCaseSensitivity()
      : PathMatcher.checkFSCaseSensitivity(target)
  );
  final Timestamp restorePoint = getAsOfTime();
  getRestoreService().restore(namePattern, restorePoint, target);
 }

 @Override
 protected void processKnownVersion(Number fileId, Number versionId)
   throws Exception
 {
  final OutputStream sink = getOutputStream();
  if (null == sink)
   getRestoreService().restore(fileId, versionId, getOutputDir());
  else
   getRestoreService().restore(fileId, versionId, sink);
 }

 public Path getOutputDir()
 {
  return outputDir;
 }

 public void setOutputDir(Path outputPath)
 {
  this.outputDir = outputPath;
 }

 public OutputStream getOutputStream()
 {
  return outputStream;
 }

 public void setOutputStream(OutputStream outputStream)
 {
  this.outputStream = outputStream;
 }

 public RestoreCommand(Manager db, ReplicaInfo replica, Configuration config)
 {
  super(db, replica, config);
 }

 protected RestoreService getRestoreService() throws DBException
 {
  if (null == restoreService)
  {
   Configuration configuration = getConfiguration();
   Manager db = getDb();
   ReplicaInfo replica = getCurrentReplica();
   restoreService = new RestoreService(db, null == replica ? null : replica.getId(), configuration);
  }
  return restoreService;
 }

 @Override
 protected void cleanup()
 {
  super.cleanup();
  if (null != restoreService)
   try
   {
    restoreService.close();
   }
   catch (Exception e)
   {
    log().log(Level.WARNING, "Close failed for the restore service", e);
   }
 }

 private Path outputDir;
 private OutputStream outputStream;
 private RestoreService restoreService;
}
